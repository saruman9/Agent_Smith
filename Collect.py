#!/usr/bin/env python
"""
This module contains function and classes
for collect information (files, directories, etc.) from PC.
"""

import os
import sys
import pathlib
import datetime
from whoosh.fields import DATETIME, TEXT, ID, SchemaClass
from whoosh.filedb.filestore import FileStorage
from whoosh.qparser import QueryParser


def print_struct(root: str='/home/neo/test'):
    """
    Print directory, files exists in `root` directory

    :param root: Root directory, it's start for a walk
    """
    for dirpath, dirnames, filenames in os.walk(root):
        print('Root: {0}'.format(dirpath))
        for name in dirnames:
            try:
                print("\tRoot contains dir: {0}".format(name))
            except UnicodeEncodeError:
                print("\tRoot contains dir: {0}".
                      format(name.encode(errors='backslashreplace')))
        for name in filenames:
            try:
                print("\tRoot contains files: {0}".format(name))
            except UnicodeEncodeError:
                print("\tRoot contains files: {0}".
                      format(name.encode(errors='backslashreplace')))


class IndexSchema(SchemaClass):
    """
    Class of Schema for indexing files.

        path: Full path to file or directory

            For example::

                /home/user/dir1/dir2/dir3/file1
                /usr/lib/python3.4/site-packages
                /dev/sdb1
                /etc

        name: Name of file or directory

           For example::

                file1, site-packages, sdb1, etc

        parent: Parent directory for file or directory

            For example::

                /home/user/dir1/dir2/dir3
                /usr/lib/python3.4
                /dev
                /

        type: Type of object

            For example::

                file, directory, symlink, socket, fifo, bdev...
    """
    path = ID(stored=True, unique=True)
    name = TEXT(stored=True)
    time = DATETIME(stored=True, sortable=True)
    parent = TEXT(stored=True)
    type = ID(stored=True)


def add_index(writer, path: str):
    """
    Add info in the index.
    :param writer: Class of index writer.
    :param path: Path to file or directory which should be add to the index.
    """
    full_path = pathlib.Path(path)
    try:
        path_index = str(full_path).encode()
        name_index = full_path.name.encode()
        parent_index = str(full_path.parent).encode()
    except UnicodeEncodeError:
        path_index = str(full_path).encode(errors='ignore')
        name_index = full_path.name.encode(errors='ignore')
        parent_index = str(full_path.parent).encode(errors='ignore')

    time_index = datetime.datetime.fromtimestamp(full_path.lstat().st_mtime)
    try:
        if full_path.is_dir():
            type_index = "dir"
        elif full_path.is_file():
            type_index = "file"
        elif full_path.is_fifo():
            type_index = "fifo"
        elif full_path.is_block_device():
            type_index = "bdev"
        elif full_path.is_socket():
            type_index = "socket"
        elif full_path.is_symlink():
            type_index = "symlink"
        elif full_path.is_char_device():
            type_index = "chardev"
        else:
            type_index = "Unknown"
    except PermissionError:
        type_index = "perm_err"

    writer.add_document(path=path_index.decode(),
                        name=name_index.decode(),
                        time=time_index,
                        parent=parent_index.decode(),
                        type=type_index)


def indexing(root: str='/home/neo/test'):
    """
    Add new file, directory in index and if it exists then
    check modification time and update.

    :param root: Root directory, it's start for a walk
    """
    ix = init_index()
    # The set of all path in the index
    indexed_paths = set()
    # The set of path to re-index
    to_index = set()

    with ix.searcher() as searcher:
        writer = ix.writer()

        # Loop over the stored fields in the index
        for fields in searcher.documents():
            indexed_path = fields['path']
            indexed_paths.add(indexed_path)

            if not os.path.exists(indexed_path):
                # File was deleted, delete from index
                writer.delete_by_term('path', indexed_path)
            else:
                # Check modification time of file
                # if new then will be re-index
                indexed_time = fields['time']
                mtime = datetime.datetime.fromtimestamp(os.path.getmtime(indexed_path))
                if mtime > indexed_time:
                    writer.delete_by_term('path', indexed_path)
                    to_index.add(indexed_path)
        writer.commit()

        for dirpath, dirnames, filenames in os.walk(root):
            writer = ix.writer()
            for name in dirnames:
                path = os.path.join(dirpath, name)
                if path in to_index or path not in indexed_paths:
                    add_index(writer, path)
            for name in filenames:
                path = os.path.join(dirpath, name)
                if path in to_index or path not in indexed_paths:
                    add_index(writer, path)
            writer.commit()


def print_contents_index(numbers: int=10):
    """
    Print contents of index.

    :param numbers: Numbers of elements for printing.
    """
    ix = init_index()
    x = 0
    with ix.searcher() as searcher:
        for doc in searcher.documents():
            x += 1
            print("Path: {0}\n"
                  "\tName: {1}\n"
                  "\tParent: {2}\n"
                  "\tTime: {3}\n"
                  "\tType: {4}".format(doc['path'],
                                       doc['name'],
                                       doc['parent'],
                                       doc['time'],
                                       doc['type']))
            if x >= int(numbers):
                break


def init_index(dir_index: str="indexdir"):
    """
    Initialize index directory and return Index class.

    :param dir_index: Name of directory, when index will be storage
    :return: Index class.
    """
    schema = IndexSchema()
    storage = FileStorage(dir_index)
    if storage.index_exists():
        ix = storage.open_index()
    else:
        storage.create()
        ix = storage.create_index(schema=schema)
    return ix


def search_index(find: str):
    """
    Search word in Index File and return dict of hits.

    :param find: String for searching.
    """
    if not find:
        return dict()

    ix = init_index()
    with ix.searcher() as searcher:
        qp = QueryParser("name", schema=ix.schema)
        q = qp.parse(find)
        results = searcher.search(q, limit=None)
        for hit in results:
            print("Path: {0}\n"
                  "\tName: {1}\n"
                  "\tParent: {2}\n"
                  "\tTime: {3}\n"
                  "\tType: {4}".format(hit['path'],
                                       hit['name'],
                                       hit['parent'],
                                       hit['time'],
                                       hit['type']))


def main():
    """
    Main function
    """
    if len(sys.argv) > 2:
        if sys.argv[1] == "search":
            search_index(sys.argv[2])
        elif sys.argv[1] == "index":
            indexing(sys.argv[2])
        elif sys.argv[1] == "print":
            print_contents_index(sys.argv[2])
        else:
            print("Error, wrong usage", file=sys.stderr)
    else:
        print("Usage: {} search [query] "
              "| print | index [path]".format(sys.argv[0]))
        # print_struct()
        # indexing()


if __name__ == '__main__':
    main()
    sys.exit()
